Título do projeto
O sistema deve permitir o cadastrar espaços de escritórios. Estes espaços podem ser catalogados como:
- Mesas de trabalho, 
- Mesas de reuniões, 
- Lounges, 
- Etc.
Estes espaços estão abertos para receber profissionais nômades que queiram um local para trabalhar ou fazer reuniões por algumas horas ou dias, dependendo da disponibilidade e aprovação da empresa.


Começando
1 - Baixar o servidor tomcat da apache: http://mirror.nbtelecom.com.br/apache/tomcat/tomcat-8/v8.5.30/bin/apache-tomcat-8.5.30.zip
2 - Descompactar o servidor em uma pasta (ex.: c:/desenvolvimento)
3 - Baixar a IDE Eclipse Oxygen: https://www.eclipse.org/downloads/download.php?file=/oomph/epp/oxygen/R2/eclipse-inst-win64.exe
4 - Instalar a IDE em uma pasta (ex.: c:/desenvolvimento)
5 - Baixar o GIT: https://git-scm.com/download/win
6 - Instalar o GIT em uma pasta (ex.: c:/desenvolvimento)
7 - Baixar o projeto: git@gitlab.com:duducordeiro/MesaLivre.git
8 - No Eclipse:
	8.1 - Importar o projeto MesaLivre através da opção "Existing Projects into Workspace"
	8.2 - Adicionar o servidor TomCat que foi descompactado nos passos anteriores e adicionar o projeto através da opção 
	8.3 - Iniciar o servidor TomCat
12 - Abrir o chrome e digitar a URL: http://localhost:8080/SimuladorPrevidencia/index.xhtml

Utilizando a aplicação
A tela inicial da aplicação mostrará 3 opções, sendo cada opção para um tipo de usuário: Administrador, RH e Profissional.
A opção Administrador permite a autenticação ou o cadastro de um novo usuário com o perfil administrador. Após se autenticar, o administrador poderá cadastrar locais para que sejam visualizados/pesquisado/locados
A opção RH permite apenas a autenticação ou o cadastro de um novo usuário com o perfil RH.
A opção Profissional ainda está em desenvolvimento

Executando os testes
Os testes da aplicação são executados via IDE Eclipse, clicando com  botão direito sobre o projeto -> Run As JUnit Tests

Sobre os testes
Os testes implementados são
Classe Teste: UserService 
	Método UserAutentication - Simula o login do usuário
	Método UserRegister - testa o cadastro de um novo usuário
	Método UserRecoveryPassword - testa a recuperação de senha enviando um e-mail ao usuário do teste

Versão 0.1

Autores
Eduardo Cordeiro - Trabalho inicial - PurpleBooth

Agradecimentos
À Vibbra por ter cedido a documentação e oportunizado este projeto