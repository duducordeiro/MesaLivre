/**
 * 
 */
package br.com.vibbra.service;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import br.com.vibbra.dao.IMesaLivreConfigurationDAO;
import br.com.vibbra.dao.IUserDAO;
import br.com.vibbra.dao.IUserTypeDAO;
import br.com.vibbra.dao.impl.FabricaDAO;
import br.com.vibbra.enums.UserTypeEnum;
import br.com.vibbra.excecao.ExcecaoMesaLivre;
import br.com.vibbra.model.MesaLivreConfiguration;
import br.com.vibbra.model.User;
import br.com.vibbra.model.UserType;

/**
 * @author Eduardo Cordeiro
 *
 */
public class UserService {

	private IUserDAO userDAO;
	private IUserTypeDAO userTypeDAO;
	private IMesaLivreConfigurationDAO mesaLivreConfigurationDAO;
	private EmailService emailService;

	/**
	 * 
	 */
	public UserService() {

	}

	/**
	 * Validates the user data to allow authentication in the application
	 * 
	 * @param userAutentication
	 * @return user
	 * @throws ExcecaoMesaLivre
	 */
	public User userAutentication(User userAutentication) throws ExcecaoMesaLivre {
		try {
			userDAO = FabricaDAO.getUserDAO();
			User user = userDAO.findUserByUserNameAndPassword(userAutentication.getUserName(), userAutentication.getPassword());
			if (user == null) {
				throw new ExcecaoMesaLivre("Usu�rio ou senha inv�lidos");
			}
			return user;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ExcecaoMesaLivre(e.getMessage());
		}
	}

	/**
	 * Validates the user data to allow authentication in the application
	 * 
	 * @param user
	 * @param administrador 
	 * @return
	 * @throws ExcecaoMesaLivre
	 */
	public void userRegister(User user, UserTypeEnum userTypeEnum) throws ExcecaoMesaLivre {
		userDAO = FabricaDAO.getUserDAO();
		userTypeDAO  = FabricaDAO.getUserTypeDAO();
		try {
			UserType userType = userTypeDAO.findById(userTypeEnum.getId());
			user.setUserType(userType);
			userDAO.makePersistent(user);
			System.out.println("Usu�rio salvo com sucesso: id:" + user.getId() + " - " + user.getUserName());
		} catch (Exception e) {
			throw new ExcecaoMesaLivre(e);
		}
	}

	public void userRecoverPassword(User userAutentication) throws ExcecaoMesaLivre {

		try {
			mesaLivreConfigurationDAO = FabricaDAO.getMesaLivreConfigurationDAO();
			MesaLivreConfiguration mesaLivreConfiguration = mesaLivreConfigurationDAO.findById(1l);

			userDAO = FabricaDAO.getUserDAO();
			User user = userDAO.findUserByUserEmail(userAutentication.getEmail());
			if (user == null) {
				throw new ExcecaoMesaLivre("N�o foi encontrado nenhum usu�rio com este e-mail: " + userAutentication.getEmail());
			}

			String hashRecoverPassword = makeSHA1Hash(user.getUserName() + new Date().getTime());
			user.setHashRecoverPassword(hashRecoverPassword);
			userDAO.makePersistent(user);

			String link = "http://localhost:8080/SimuladorPrevidencia/paginas/cadastrarNovaSenha.xhtml?hash=" + hashRecoverPassword;
			String emailMessage = mesaLivreConfiguration.getMessageEmailRecoveryPassword() + "\n\n " + link;

			emailService = new EmailService();
			emailService.sendMail(mesaLivreConfiguration.getEmailCompany(), user.getEmail(), mesaLivreConfiguration.getSubjectEmailRecoveryPassword(), emailMessage,
					mesaLivreConfiguration.getPasswordEmailCompany(), hashRecoverPassword);
		} catch (ExcecaoMesaLivre e) {
			e.printStackTrace();
			throw e;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw new ExcecaoMesaLivre("Problemas no algoritimo de cria��o do hash");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new ExcecaoMesaLivre("Problemas no encoding utilizado na cria��o do hash");
		} catch (Exception e) {
			e.printStackTrace();
			throw new ExcecaoMesaLivre("Erro ao finalizar a o processo de recuperar a senha");
		}

	}

	public void registerNewPassword(User userAutentication) throws ExcecaoMesaLivre {
		userDAO = FabricaDAO.getUserDAO();
		try {
			User user = userDAO.findUserByUHashRecoverPassword(userAutentication.getHashRecoverPassword());
			if (user == null) {
				throw new ExcecaoMesaLivre("Problemas ao recuperar o usu�rio do banco de dados");
			}
			user.setPassword(userAutentication.getPassword());
			userDAO.makePersistent(user);
			user.setHashRecoverPassword(null);
			userDAO.makePersistent(user);
			System.out.println("Usu�rio salvo com sucesso: id:" + user.getId() + " - " + user.getUserName());
		} catch (ExcecaoMesaLivre e) {
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ExcecaoMesaLivre("Erro ao finalizar a o processo de salvar nova senha");
		}
	}
	
	public User getUserByHashPassword(String hashRecoverPassword) throws ExcecaoMesaLivre {
		User user;
		try {
			userDAO = FabricaDAO.getUserDAO();
			user = userDAO.findUserByUHashRecoverPassword(hashRecoverPassword);
			if (user == null) {
				throw new ExcecaoMesaLivre("N�o foi localizada a solicita��o de altera��o de senha para este link");
			}
			return user;
		} catch (ExcecaoMesaLivre e) {
			e.printStackTrace();
			throw e;
		}
	}


	public String makeSHA1Hash(String input) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest md = MessageDigest.getInstance("SHA1");
		md.reset();
		byte[] buffer = input.getBytes("UTF-8");
		md.update(buffer);
		byte[] digest = md.digest();

		String hexStr = "";
		for (int i = 0; i < digest.length; i++) {
			hexStr += Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1);
		}
		return hexStr;
	}

}
