/**
 * 
 */
package br.com.vibbra.service;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

import br.com.vibbra.excecao.ExcecaoMesaLivre;

/**
 * @author Eduardo Cordeiro
 *
 */
public class EmailService {

	public void sendMail(String emailFrom, String emailTo, String subject, String message, String passwordEmailCompany, String hashRecoverPassword) throws ExcecaoMesaLivre {
		try {
			Email email = new SimpleEmail();
			email.setHostName("smtp.googlemail.com");
			email.setSmtpPort(465);
			email.setAuthenticator(new DefaultAuthenticator(emailFrom, passwordEmailCompany));
			email.setSSLOnConnect(true);
			email.setFrom(emailFrom);
			email.setSubject(subject);
			email.setMsg(message);
			email.addTo(emailTo);
			email.send();
			System.out.println("E-mail enviado com sucesso a: " + emailTo);
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			throw new ExcecaoMesaLivre(e);
		}
	}
	
	public static void main(String[] args) {
		try {
			new EmailService().sendMail("eduardo.cordeiro999@gmail.com", "duducordeiro_@hotmail.com","1123", "321", "pqmoqzzhjgfehvru", "hash");
			
//			Email email = new SimpleEmail();
//			email.setHostName("smtp.googlemail.com");
//			email.setSmtpPort(465);
//			email.setAuthenticator(new DefaultAuthenticator("eduardo.cordeiro999@gmail.com", "pqmoqzzhjgfehvru"));
//			email.setSSLOnConnect(true);
//			email.setFrom("eduardo.cordeiro999@gmail.com");
//			email.setSubject("TestMail");
//			email.setMsg("This is a test mail ... :-)");
//			email.addTo("duducordeiro_@hotmail.com");
//			email.send();
			
			System.out.println("sucesso");
		} catch (ExcecaoMesaLivre e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
