/**
 * 
 */
package br.com.vibbra.tests;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import br.com.vibbra.model.City;
import br.com.vibbra.model.MesaLivreConfiguration;
import br.com.vibbra.model.State;
import br.com.vibbra.model.User;
import br.com.vibbra.model.UserType;

/**
 * @author Eduardo Cordeiro
 *
 */
public class TestHibernateConnection {

	public static void main(String[] args) {
		createDataTests();
	}

	/**
	 * 
	 */
	protected static void createDataTests() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("MesaLivrePU");
		EntityManager em = emf.createEntityManager();

		State state = createState(em, "SC", "Santa Catarina");
		System.out.println(state.getId() + " - " + state.getNome());

		state = createState(em, "PR", "Paran�");
		System.out.println(state.getId() + " - " + state.getNome());

		City city = createCity(em, "Florian�polis", "SC");
		System.out.println(city.getId() + " - " + city.getNome());

		UserType typeAdministrator = createUserType(em, "Administrador");
		UserType typeRH = createUserType(em, "RH");
		UserType typeProfessional = createUserType(em, "Profissional");

		createDefaultUser(em, typeAdministrator, "adm", "22.222.222/2222-21", "duducordeiro_@hotmail.com");
		createDefaultUser(em, typeRH, "rh", "22.222.222/2222-22", "eduardojsc@outlook.com.br");
		createDefaultUser(em, typeProfessional, "prof", "009.362.739-41", "eduardojscordeiro@outlook.com");
		
		createConfigurations(em);
	}

	private static void createConfigurations(EntityManager em) {
		MesaLivreConfiguration configuration = new MesaLivreConfiguration();
		configuration.setEmailCompany("eduardo.cordeiro999@gmail.com");
		configuration.setMessageEmailRecoveryPassword("Abaixo segue o link que voc� deve digitar para cadastrar uma nova senha:");
		configuration.setPasswordEmailCompany("pqmoqzzhjgfehvru");
		configuration.setSubjectEmailRecoveryPassword("[Mesa Livre] - Recupera��o de senha");
		
		em.getTransaction().begin();
		em.persist(configuration);
		em.getTransaction().commit();
		System.out.println("Configura��es cadastradas com sucesso: "+configuration.toString());
	}

	private static void createDefaultUser(EntityManager em, UserType typeA, String userName, String document, String email) {
		User user = new User();
		user.setDocument(document);
		user.setName("Vibbra");
		user.setEmail(email);
		user.setPassword("123456");
		user.setUserName(userName);
		user.setUserType(typeA);

		em.getTransaction().begin();
		em.persist(user);
		em.getTransaction().commit();
		System.out.println("Create User id: " + user.getId() + " - " + user.getUserName());
	}

	private static UserType createUserType(EntityManager em, String string) {
		UserType type = new UserType();
		type.setDescription(string);

		em.getTransaction().begin();
		em.persist(type);
		em.getTransaction().commit();
		System.out.println("Create UserType id: " + type.getId() + " - " + type.getDescription());
		return type;
	}

	private static State createState(EntityManager em, String initial, String stateName) {
		State state;
		state = new State();
		state.setNome(stateName);
		state.setSigla(initial);
		em.getTransaction().begin();
		em.persist(state);
		em.getTransaction().commit();
		return state;
	}

	private static City createCity(EntityManager em, String nameCity, String initialsState) {
		City city = new City();

		Query query = em.createNamedQuery("State.findBySigla");
		query.setParameter("sigla", initialsState);
		State e = (State) query.getSingleResult();
		city.setEstado(e);
		city.setNome(nameCity);

		em.getTransaction().begin();
		em.persist(city);
		em.getTransaction().commit();
		return city;
	}
}
