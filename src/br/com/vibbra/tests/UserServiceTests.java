package br.com.vibbra.tests;

import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import br.com.vibbra.dao.impl.UserDAO;
import br.com.vibbra.enums.UserTypeEnum;
import br.com.vibbra.excecao.ExcecaoMesaLivre;
import br.com.vibbra.model.User;
import br.com.vibbra.service.UserService;

public class UserServiceTests {
	


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		TestHibernateConnection.createDataTests();
	}

	@Test
	public void testUserAutentication() {
		UserService service = new UserService();
		User user = new User();
		user.setUserName("wrongUserName");
		user.setPassword("wrongPassword");
		try {
			user = service.userAutentication(user);
		} catch (ExcecaoMesaLivre e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			assertTrue(true);
		}
	}

	@Test
	public void testUserRegister() {
		UserService service = new UserService();
		User user = new User();
		user.setDocument("22.222.222/2222-41");
		user.setName("Vibbra Junit");
		user.setEmail("duducordeiro123@hotmail.com");
		user.setPassword("123456");
		user.setUserName("eduardo");
		user.setHashRecoverPassword("123456");

		try {
			service.userRegister(user, UserTypeEnum.ADMINISTRADOR);
			assertTrue(user != null);
		} catch (ExcecaoMesaLivre e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testUserRecoverPassword() {
		try {
			UserService service = new UserService();
			UserDAO userDAO = new UserDAO();
			User user = userDAO.findUserByUserEmail("duducordeiro_@hotmail.com");
			assertTrue(user.getHashRecoverPassword() == null);
		
			service.userRecoverPassword(user);
			user = service.userAutentication(user);
			assertTrue(user.getHashRecoverPassword() != null);
		} catch (ExcecaoMesaLivre e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
