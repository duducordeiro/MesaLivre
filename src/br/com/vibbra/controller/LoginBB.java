/**
 * 
 */
package br.com.vibbra.controller;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import br.com.vibbra.enums.UserTypeEnum;
import br.com.vibbra.excecao.ExcecaoMesaLivre;
import br.com.vibbra.model.User;
import br.com.vibbra.service.UserService;

/**
 * @author Eduardo Cordeiro
 *
 */

@RequestScoped
@ManagedBean
public class LoginBB {

	private User user;
	private User userAutentication;
	private User userRegisterPassword;
	private UserService userService;
	private boolean exibirLinkLogin;

	public LoginBB() {
		System.out.println("Instanciando LoginBB");
		initializateObjects();
		verifciarURL();
	}


	private void initializateObjects() {
		user = new User();
		userAutentication = new User();
		userService = new UserService();
	}

	public String autentication() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			user = userService.userAutentication(userAutentication);
			context.getExternalContext().getSessionMap().put("user", user);
			return "/paginas/cadastroLocais";
		} catch (ExcecaoMesaLivre e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setErrorMessageInterface("Erro ao logar", e.getMessage());
			return "";
		}
	}

	public void registerAdministrador() {
		userRegister(UserTypeEnum.ADMINISTRADOR);
	}
	
	public void registerRH() {
		userRegister(UserTypeEnum.RH);
	}


	/**
	 * 
	 */
	private String userRegister(UserTypeEnum userTypeEnum) {
		try {
			userService.userRegister(user, userTypeEnum);
			setInfoMessageInterface("Cadastro", "Usu�rio salvo com sucesso");
			return "/index";
		} catch (ExcecaoMesaLivre e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setErrorMessageInterface("Cadastro", "Erro ao salvar.");
			return "";
		}
	}
	
	private void setErrorMessageInterface(String title, String messagem) {
		setMessageInterface(title, messagem, FacesMessage.SEVERITY_ERROR);
	}
	
	private void setInfoMessageInterface(String title, String messagem) {
		setMessageInterface(title, messagem, FacesMessage.SEVERITY_INFO);
	}
	

	private void setMessageInterface(String title, String messagem, Severity severity) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(severity, title, messagem));
	}

	@PostConstruct
	public void init() {
		System.out.println(" Instantiated Bean! ");
	}

	public String changePassword() {
		try {
			userService.userRecoverPassword(userAutentication);
			setInfoMessageInterface("Recupera��o de Senha", "Um link para cadastrar uma nova senha foi enviado para o seu e-mail.");
			return "/index";
		} catch (ExcecaoMesaLivre e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setErrorMessageInterface("Recupera��o de Senha", "Erro ao enviar o link para o e-mail:" + e.getMessage());
			return "";
		}
	}
	
	public String registerNewPassword() {
		try {
			String hash = FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("hash").toString();
			userAutentication.setHashRecoverPassword(hash);
			userService.registerNewPassword(userAutentication);
			setInfoMessageInterface("Altera��o de Senha", "Senha alterada com sucesso.");
			exibirLinkLogin = true;
			return "/index";
		} catch (ExcecaoMesaLivre e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setErrorMessageInterface("Recupera��o de Senha", "Erro ao enviar o link para o e-mail: " + e.getMessage());
			return "";
		}
	}

	private void verifciarURL() {
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			Map<String, String> paramMap = context.getExternalContext().getRequestParameterMap();
			String hash = paramMap.get("hash");
			if(hash != null) {
				userRegisterPassword = userService.getUserByHashPassword(hash);
				context.getExternalContext().getSessionMap().put("user", userAutentication);
				context.getExternalContext().getSessionMap().put("hash", hash);
			}
		} catch (ExcecaoMesaLivre e) {
			e.printStackTrace();
			setErrorMessageInterface("Recupera��o de Senha", e.getMessage());
		}
		
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getUserAutentication() {
		if (userAutentication == null) {
			userAutentication = new User();
		}
		return userAutentication;
	}

	public void setUserAutentication(User userAutentication) {
		this.userAutentication = userAutentication;
	}

	public User getUserRegisterPassword() {
		return userRegisterPassword;
	}

	public void setUserRegisterPassword(User userRegisterPassword) {
		this.userRegisterPassword = userRegisterPassword;
	}


	public boolean isExibirLinkLogin() {
		return exibirLinkLogin;
	}


	public void setExibirLinkLogin(boolean exibirLinkLogin) {
		this.exibirLinkLogin = exibirLinkLogin;
	}

	

}
