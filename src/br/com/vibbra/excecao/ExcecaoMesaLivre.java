package br.com.vibbra.excecao;

import java.io.Serializable;

/**
 * 
 * @author Eduardo Cordeiro
 *
 */
public class ExcecaoMesaLivre extends Exception implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ExcecaoMesaLivre(String message) {
		super(message);
	}

	public ExcecaoMesaLivre(Throwable throwable) {
		super(throwable);
	}

	public ExcecaoMesaLivre(String message, Throwable throwable) {
		super(message, throwable);
	}

	public ExcecaoMesaLivre(Throwable throwable, String message) {
		super(message, throwable);
	}

}
