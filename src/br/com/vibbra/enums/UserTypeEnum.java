/**
 * 
 */
package br.com.vibbra.enums;

/**
 * @author Eduardo Cordeiro
 *
 */
public enum UserTypeEnum {

	ADMINISTRADOR(1,"Administrador"),
	RH(1,"RH"),
	PROFISSIONAL(1,"Profissional");

	private long id;
	private String descricao;
	
	private UserTypeEnum(int id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
	

}
