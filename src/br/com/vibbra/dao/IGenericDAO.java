/**
 * 
 */
package br.com.vibbra.dao;

import java.io.Serializable;
import java.util.List;

/**
 * @author Eduardo Cordeiro
 *
 */
public interface IGenericDAO<T, ID extends Serializable> {

	T findById(ID id);

	List<T> findAll(String colunaOrdenacao);

	T makePersistent(T entity) throws Exception;

	void makeTransient(T entity);

	Long count();

}
