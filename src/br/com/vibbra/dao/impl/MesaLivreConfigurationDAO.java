/**
 * 
 */
package br.com.vibbra.dao.impl;

import br.com.vibbra.dao.IMesaLivreConfigurationDAO;
import br.com.vibbra.model.MesaLivreConfiguration;

/**
 * @author Eduardo Cordeiro
 *
 */
public class MesaLivreConfigurationDAO extends GenericDAO<MesaLivreConfiguration, Long> implements IMesaLivreConfigurationDAO {

}
