/**
 * 
 */
package br.com.vibbra.dao.impl;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.vibbra.dao.IUserDAO;
import br.com.vibbra.excecao.ExcecaoMesaLivre;
import br.com.vibbra.model.User;
import br.com.vibbra.session.HibernateUtil;

/**
 * @author Eduardo Cordeiro
 *
 */
public class UserDAO extends GenericDAO<User, Long> implements IUserDAO {

	/**
	 * 
	 */
	public UserDAO() {
		// TODO Auto-generated constructor stub
	}


	@Override
	public User findUserByUserName(String name) throws ExcecaoMesaLivre {
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			Transaction transaction = null;
			transaction = session.beginTransaction();

			Query query = session.getNamedQuery("User.findByUserName");
			query.setParameter("login", name);
			User user = (User) query.uniqueResult();

			transaction.commit();
			session.close();

			return user;
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new ExcecaoMesaLivre("Erro no banco de dados ao recuperar o usu�rio");
		}
	}

	@Override
	public User findUserByUserNameAndPassword(String userName, String password) throws ExcecaoMesaLivre {
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			Transaction transaction = null;
			transaction = session.beginTransaction();

			Query query = session.getNamedQuery("User.findByUserNameAndPassword");
			query.setParameter("userName", userName);
			query.setParameter("password", password);
			User user = (User) query.uniqueResult();

			transaction.commit();
			session.close();

			return user;
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new ExcecaoMesaLivre("Erro no banco de dados ao recuperar o usu�rio");
		}
	}

	@Override
	public User findUserByUserEmail(String email) throws ExcecaoMesaLivre {
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			Transaction transaction = null;
			transaction = session.beginTransaction();

			Query query = session.getNamedQuery("User.findByEmail");
			query.setParameter("email", email);
			User user = (User) query.uniqueResult();

			transaction.commit();
			session.close();

			return user;
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new ExcecaoMesaLivre("Erro no banco de dados ao recuperar o e-mail do usu�rio");
		}
	}


	@Override
	public User findUserByUHashRecoverPassword(String hashRecoverPassword) throws ExcecaoMesaLivre {
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			Transaction transaction = null;
			transaction = session.beginTransaction();

			Query query = session.getNamedQuery("User.findByHashRecoverPassword");
			query.setParameter("hashRecoverPassword", hashRecoverPassword);
			User user = (User) query.uniqueResult();

			transaction.commit();
			session.close();

			return user;
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new ExcecaoMesaLivre("Erro no banco de dados ao recuperar o e-mail do usu�rio");
		}
	}

}
