package br.com.vibbra.dao.impl;

import br.com.vibbra.dao.IMesaLivreConfigurationDAO;
import br.com.vibbra.dao.IUserDAO;
import br.com.vibbra.dao.IUserTypeDAO;

/**
 * 
 * @author Eduardo Cordeiro
 *
 */
public class FabricaDAO {

	private static IUserDAO user;
	private static IUserTypeDAO userType;
	private static IMesaLivreConfigurationDAO mesaLivreConfiguration;

	public static IUserDAO getUserDAO() {
		if (user == null) {
			user = new UserDAO();
		}
		return user;
	}

	public static IUserTypeDAO getUserTypeDAO() {
		if (userType == null) {
			userType = new UserTypeDAO();
		}
		return userType;
	}

	public static IMesaLivreConfigurationDAO getMesaLivreConfigurationDAO() {
		if (mesaLivreConfiguration == null) {
			mesaLivreConfiguration = new MesaLivreConfigurationDAO();
		}
		return mesaLivreConfiguration;
	}

}
