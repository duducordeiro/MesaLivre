/**
 * 
 */
package br.com.vibbra.dao.impl;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.vibbra.dao.IUserTypeDAO;
import br.com.vibbra.model.UserType;
import br.com.vibbra.session.HibernateUtil;

/**
 * @author Eduardo Cordeiro
 *
 */
public class UserTypeDAO extends GenericDAO<UserType, Long> implements IUserTypeDAO {

	/**
	 * 
	 */
	public UserTypeDAO() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public UserType findUserTypeByDescription(String description) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		transaction = session.beginTransaction();

		Query query = session.getNamedQuery("UserType.findByDescription");
		query.setParameter("description", description);
		UserType user = (UserType) query.uniqueResult();

		transaction.commit();
		session.close();

		return user;
	}

}