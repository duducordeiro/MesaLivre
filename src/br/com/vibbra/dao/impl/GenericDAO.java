/**
 * 
 */
package br.com.vibbra.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;

import br.com.vibbra.dao.IGenericDAO;
import br.com.vibbra.session.HibernateUtil;

/**
 * @author Eduardo Cordeiro
 *
 */
public abstract class GenericDAO<T, ID extends Serializable> implements IGenericDAO<T, ID> {

	private Class<T> persistentClass;

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	public GenericDAO() {
		this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	public Class<T> getPersistentClass() {
		return persistentClass;
	}

	@SuppressWarnings("unchecked")
	public T findById(ID id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		T entity = (T) session.get(getPersistentClass(), id);
		transaction.commit();
		session.close();
		return entity;
	}

	public List<T> findAll(String colunaOrdenacao) {
		return findByCriteria(Order.asc(colunaOrdenacao));
	}

	public T makePersistent(T entity) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();

		session.saveOrUpdate(entity);

		transaction.commit();
		session.close();

		return entity;
	}

	public void makeTransient(T entity) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();

		session.delete(entity);

		transaction.commit();
		session.close();

	}

	/**
	 * Use this inside subclasses as a convenience method.
	 */
	@SuppressWarnings("unchecked")
	protected List<T> findByCriteria(Order ordem, Criterion... criterion) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();

		Criteria crit = session.createCriteria(getPersistentClass()).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		if (ordem != null) {
			crit.addOrder(ordem);
		}
		for (Criterion c : criterion) {
			crit.add(c);
		}
		List<T> list = crit.list();

		transaction.commit();
		session.close();

		return list;
	}

	public Long count() {
		Long quantidade = 0l;

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();

		quantidade = (Long) session.createCriteria(getPersistentClass()).setProjection(Projections.rowCount()).uniqueResult();

		transaction.commit();
		session.close();

		return quantidade;
	}
}
