package br.com.vibbra.dao;

import br.com.vibbra.model.UserType;

public interface IUserTypeDAO extends IGenericDAO<UserType, Long> {

	/**
	 * Return the user object searching by userName
	 * 
	 * @param usarName
	 * @return
	 */
	public UserType findUserTypeByDescription(String usarName);

}
