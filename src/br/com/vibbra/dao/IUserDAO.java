package br.com.vibbra.dao;

import br.com.vibbra.excecao.ExcecaoMesaLivre;
import br.com.vibbra.model.User;

public interface IUserDAO extends IGenericDAO<User, Long> {

	/**
	 * Return the user object searching by userName
	 * 
	 * @param usarName
	 * @return
	 */
	public User findUserByUserName(String usarName) throws ExcecaoMesaLivre;

	/**
	 * Return the user object searching by userName and password
	 * 
	 * @param usarName
	 * @return
	 */
	public User findUserByUserNameAndPassword(String userName, String password) throws ExcecaoMesaLivre;

	/**
	 * Return the user object searching by e-mail
	 * 
	 * @param email
	 * @return
	 * @throws ExcecaoMesaLivre 
	 */
	public User findUserByUserEmail(String email) throws ExcecaoMesaLivre;
	
	/**
	 * Return the user object searching by hashRecoverPassword
	 * 
	 * @param email
	 * @return
	 * @throws ExcecaoMesaLivre 
	 */
	public User findUserByUHashRecoverPassword(String email) throws ExcecaoMesaLivre;

}
