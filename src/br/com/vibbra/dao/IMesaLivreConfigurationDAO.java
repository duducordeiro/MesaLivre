/**
 * 
 */
package br.com.vibbra.dao;

import br.com.vibbra.model.MesaLivreConfiguration;

/**
 * @author Eduardo Cordeiro
 *
 */
public interface IMesaLivreConfigurationDAO extends IGenericDAO<MesaLivreConfiguration, Long> {

}