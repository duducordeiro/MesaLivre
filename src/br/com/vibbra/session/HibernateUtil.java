package br.com.vibbra.session;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import javax.faces.bean.ApplicationScoped;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.service.ServiceRegistry;

import br.com.vibbra.implementation.VariavelConexaoUtil;

/**
 * Responsible for establishing a data connection with Hibernate
 * 
 * @author Eduardo Cordeiro
 *
 */

@ApplicationScoped
public class HibernateUtil implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static String JAVA_COMP_ENV_JDBC_DATASOURCE = "java:/comp/env/jdbc/datasource";

	private static SessionFactory sessionFactory = buildSessionFactory();

	/**
	 * Responsible for reading the configuration file (hibernate.cfg.xml)
	 * 
	 * @return
	 */
	private static SessionFactory buildSessionFactory() {

		try {
			if (sessionFactory == null) {
				Configuration configuration = new Configuration();
				configuration.configure();
				ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
				sessionFactory = configuration.buildSessionFactory(serviceRegistry);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ExceptionInInitializerError(e);
		}

		return sessionFactory;
	}

	/**
	 * Return the session factory to get current session
	 * @return
	 */
	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			buildSessionFactory();
		}
		return sessionFactory;
	}

	/**
	 * return de current session 
	 * @return
	 */
	public static Session getCurrentSession() {
		return getSessionFactory().getCurrentSession();
	}

	public static Session openSession() throws Exception {
		if (sessionFactory == null) {
			buildSessionFactory();
		}

		return sessionFactory.openSession();
	}

	/**
	 * getConnectionProvider does not work for a multi tenant setup
	 * 
	 * @return
	 * @throws SQLException
	 */
	@SuppressWarnings("deprecation")
	public static Connection getConnectionProvider() throws SQLException {
		return ((SessionFactoryImplementor) sessionFactory).getConnectionProvider().getConnection();
	}

	/**
	 * 
	 * @return Connection in InitialContext 'java:/comp/env/jdbc/datasource'
	 * @throws Exception
	 */
	public static Connection getConnection() throws Exception {
		InitialContext context = new InitialContext();
		DataSource ds = (DataSource) context.lookup(JAVA_COMP_ENV_JDBC_DATASOURCE);

		return ds.getConnection();
	}

	/**
	 * Datasource for running unit tests
	 * 
	 * @return
	 * @throws NamingException
	 */
	public DataSource getDataSourceJndi() throws NamingException {
		InitialContext initialContext = new InitialContext();
		return (DataSource) initialContext.lookup(VariavelConexaoUtil.JAVA_COMP_ENV_JDBC_DATASOURCE);
	}

}
