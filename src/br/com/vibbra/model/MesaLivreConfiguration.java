/**
 * 
 */
package br.com.vibbra.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Eduardo Cordeiro
 *
 */
@Entity
@Table(name = "mesalivreConfiguration", schema = "public")
public class MesaLivreConfiguration {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	private Long id;

	@Basic(optional = false)
	@Column(name = "emailCompany")
	private String emailCompany;

	@Basic(optional = false)
	@Column(name = "subjectEmailRecoveryPassword")
	private String subjectEmailRecoveryPassword;

	@Basic(optional = false)
	@Column(name = "messageEmailRecoveryPassword")
	private String messageEmailRecoveryPassword;

	@Basic(optional = false)
	@Column(name = "passwordEmailCompany")
	private String passwordEmailCompany;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmailCompany() {
		return emailCompany;
	}

	public void setEmailCompany(String emailCompany) {
		this.emailCompany = emailCompany;
	}

	public String getSubjectEmailRecoveryPassword() {
		return subjectEmailRecoveryPassword;
	}

	public void setSubjectEmailRecoveryPassword(String subjectEmailRecoveryPassword) {
		this.subjectEmailRecoveryPassword = subjectEmailRecoveryPassword;
	}

	public String getMessageEmailRecoveryPassword() {
		return messageEmailRecoveryPassword;
	}

	public void setMessageEmailRecoveryPassword(String messageEmailRecoveryPassword) {
		this.messageEmailRecoveryPassword = messageEmailRecoveryPassword;
	}

	public String getPasswordEmailCompany() {
		return passwordEmailCompany;
	}

	public void setPasswordEmailCompany(String passwordEmailCompany) {
		this.passwordEmailCompany = passwordEmailCompany;
	}

	@Override
	public String toString() {
		return "MesaLivreConfiguration [id=" + id + ", emailCompany=" + emailCompany + ", subjectEmailRecoveryPassword=" + subjectEmailRecoveryPassword + ", messageEmailRecoveryPassword="
				+ messageEmailRecoveryPassword + ", passwordEmailCompany=" + passwordEmailCompany + "]";
	}
	
	

}
