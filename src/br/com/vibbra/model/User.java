/**
 * 
 */
package br.com.vibbra.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Eduardo Cordeiro
 *
 */

@Entity
@Table(name = "user_system", schema = "public")
@NamedQueries({ @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"), @NamedQuery(name = "User.findById", query = "SELECT u FROM User u WHERE u.id = :id"),
		@NamedQuery(name = "User.findByEmail", query = "SELECT u FROM User u WHERE u.email = :email"),
		@NamedQuery(name = "User.findByUserNameAndPassword", query = "SELECT u FROM User u WHERE u.userName = :userName and u.password = :password"),
		@NamedQuery(name = "User.findByHashRecoverPassword", query = "SELECT u FROM User u WHERE u.hashRecoverPassword = :hashRecoverPassword"),
		@NamedQuery(name = "User.findByUserName", query = "SELECT u FROM User u WHERE u.userName = :userName") })
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	private Long id;

	@Basic(optional = false)
	@Column(name = "userName", unique = true)
	private String userName;

	@Basic(optional = false)
	@Column(name = "name")
	private String name;
	
	@Basic(optional = false)
	@Column(name = "document", unique = true)
	private String document;

	@Basic(optional = false)
	@Column(name = "email", unique = true)
	private String email;

	@Basic(optional = false)
	@Column(name = "password")
	private String password;

	@JoinColumn(name = "userType", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private UserType userType;

	@Basic(optional = true)
	@Column(name = "hashRecoverPassword")
	private String hashRecoverPassword;
	
	@Basic(optional = true)
	@Column(name = "telefone")
	private String telefone;

	/**
	 * 
	 */
	public User() {
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public String getHashRecoverPassword() {
		return hashRecoverPassword;
	}

	public void setHashRecoverPassword(String hashRecoverPassword) {
		this.hashRecoverPassword = hashRecoverPassword;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
}
