/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.vibbra.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * 
 * @author Eduardo Cordeiro
 *
 */
@Entity
@Table(name = "city", schema = "public")
@NamedQueries({ @NamedQuery(name = "City.findAll", query = "SELECT c FROM City c"), @NamedQuery(name = "City.findById", query = "SELECT c FROM City c WHERE c.id = :id"),
		@NamedQuery(name = "City.findByNome", query = "SELECT c FROM City c WHERE c.nome = :nome") })
public class City implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	private Long id;

	@Basic(optional = false)
	@Column(name = "nome")
	private String nome;

	@JoinColumn(name = "state", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private State state;

	public City() {
	}

	public City(Long id) {
		this.id = id;
	}

	public City(Long id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public State getState() {
		return state;
	}

	public void setEstado(State state) {
		this.state = state;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof City)) {
			return false;
		}
		City other = (City) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "br.com.empresa.entidade.City[ id=" + id + " ]";
	}

}
