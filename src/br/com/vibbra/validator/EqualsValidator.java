/**
 * 
 */
package br.com.vibbra.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * 
 * @author Eduardo Cordeiro
 *
 */
@FacesValidator(value = "equalsValidator")
public class EqualsValidator implements Validator {

	
	
	@Override
	public void validate(FacesContext arg0, UIComponent component, Object value) throws ValidatorException {
		Object otherValue = component.getAttributes().get("otherValue");

		if (value == null || otherValue == null) {
			return; // Let required="true" handle.
		}

		String pattern = "(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,}";
		System.out.println(value.toString().matches(pattern));
		if (!value.toString().matches(pattern)) {
			FacesMessage  msg = new FacesMessage("Erro na valida��o da senha", "A senha deve conter ao menos uma letra mai�scula, um n�mero e um caracter especial (@#$%^&+=).");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}

		if (!value.equals(otherValue)) {
//			setMessageInterface("Erro na valida��o da senha", "As senhas digitadas n�o conferem.");
			FacesMessage  msg = new FacesMessage("Erro na valida��o da senha", "As senhas digitadas n�o conferem.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
	}
	
	public static void main(String[] args) {
//		String passwd2 = "aaZZa44@";
		String passwd = "A1@123456";
		String pattern = "(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,}";
		System.out.println(passwd.toString().matches(pattern));
		if (passwd.toString().matches(pattern)) {
			System.out.println("True");
		} else {
			System.out.println("false");
		}
	}

	public static void ma2in(String[] args) {
		String passwd = "aaZZa44@";
		String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
		System.out.println(passwd.matches(pattern));
	}

}
