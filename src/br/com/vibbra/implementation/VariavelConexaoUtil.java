/**
 * 
 */
package br.com.vibbra.implementation;

/**
 * JNDI Datasource Name
 * 
 * @author Eduardo Cordeiro
 *
 */
public class VariavelConexaoUtil {

	public static String JAVA_COMP_ENV_JDBC_DATASOURCE = "java:/comp/env/jdbc/datasource";

}
