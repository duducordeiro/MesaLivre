Project 
The system should allow you to register office spaces. These spaces can be cataloged as:
- Work tables,
- Meeting tables,
- Lounges,
- Etc.
These spaces are open to receive nomadic professionals who want a place to work or hold meetings for a few hours or days, depending on the availability and approval of the company.

Starting
1 - Download the tomcat server from apache: http://mirror.nbtelecom.com.br/apache/tomcat/tomcat-8/v8.5.30/bin/apache-tomcat-8.5.30.zip
2 - Unpack the server into a folder (eg c:/development)
3 - Download the IDE Eclipse Oxygen: https://www.eclipse.org/downloads/download.php?file=/oomph/epp/oxygen/R2/eclipse-inst-win64.exe
4 - Install the IDE in a folder (eg c:/development)
5 - Download the GIT: https://git-scm.com/download/win
6 - Install the GIT into a folder (eg c:/development)
7 - Download the project: git@gitlab.com: duducordeiro / MesaLivre.git
8 - In Eclipse:
8.1 - Import the project MesaLivre through the option "Existing Projects into Workspace"
8.2 - Add the TomCat server that was unpacked in the previous steps and add the project through the option
8.3 - Starting the TomCat server
9 - Open the chrome and enter the URL: http://localhost:8080/SimuladorPrevidencia/index.xhtml

Using the application
The initial screen of the application will show 3 options, being each option for a type of user: Administrator, HR and Professional.
The Administrator option allows the authentication or registration of a new user with the administrator profile. After being authenticated, the administrator can register locations for viewing / searching / leased
The RH option allows only the authentication or registration of a new user with the RH profile.
The Professional option is still under development

Running the tests
The application tests are run via Eclipse IDE, by clicking with the right button on the project -> "Run As JUnit Tests"

About the tests
The implemented tests are
Test Class: UserServiceTests
UserAutentication Method - Simulates user login
UserRegister Method - Tests the registration of a new user
UserRecoveryPassword method - Tests password recovery by sending an email to the test user

Version 0.1

Authors
Eduardo Cordeiro - Initial work

Thanks
To Vibbra for having given the documentation and opportunized this project.
